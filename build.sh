#! /bin/bash

# Build JavaScript

nix-build --arg isJS true

# Copy to JS folder

cp result/bin/shpaboinchkle.jsexe/all.min.js js/all.min.js
chmod a+w js/all.min.js
